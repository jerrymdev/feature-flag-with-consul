package com.example.consul.lib;

import com.example.consul.api.TestSingleton;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class FileChangeListener {

    private final TestSingleton testSingleton;

    public FileChangeListener(TestSingleton testSingleton) {
        this.testSingleton = testSingleton;
    }

    public HashMap<String, Boolean> getToggles() throws IOException, InterruptedException {
        WatchService watchService = FileSystems.getDefault().newWatchService();
        Path path = Paths.get("/opt/consul-feature-toggle");
        path.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
        WatchKey key;

        HashMap<String, Boolean> toggles = new HashMap<>();

        while ((key = watchService.take()) != null) {
            for (WatchEvent<?> event : key.pollEvents()) {
                System.out.println("Event kind:" + event.kind() + ". File affected: " + event.context() + ".");
            }

            key.reset();

            toggles = readFile();

            return toggles;
        }

        watchService.close();
        return toggles;
    }

    public HashMap<String, Boolean> readFile() throws IOException {
        HashMap<String, Boolean> togglesMap = new HashMap();

        String fileName = "/opt/consul-feature-toggle/toggles";
        Path path = Paths.get(fileName);
        List<String> allLines = Files.readAllLines(path, StandardCharsets.UTF_8);
        var toggles = allLines.stream()
                .filter(s -> s.trim() != "")
                .map(s -> s.trim().toUpperCase()).collect(Collectors.toList());

        for (String toggle:toggles) {
            String[] parts = toggle.split("=",2);
            togglesMap.put(parts[0],Boolean.valueOf(parts[1]));
        }
        return togglesMap;
    }

}
