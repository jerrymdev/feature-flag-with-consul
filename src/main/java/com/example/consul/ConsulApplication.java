package com.example.consul;

import com.example.consul.api.TestSingleton;
import com.example.consul.api.TogglesState;
import com.example.consul.api_admin.ManageToggle;
import com.example.consul.lib.FileChangeListener;
import com.orbitz.consul.*;
import com.orbitz.consul.model.agent.ImmutableRegistration;
import com.orbitz.consul.model.agent.Registration;
import com.orbitz.consul.model.health.ServiceHealth;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@SpringBootApplication
public class ConsulApplication implements CommandLineRunner {
	private final FileChangeListener fileChangeListener;
	private final ManageToggle manageToggle;

	private final TogglesState togglesState;
	private final TestSingleton testSingleton;

	public ConsulApplication(FileChangeListener fileChangeListener, ManageToggle manageToggle, TogglesState togglesState, TestSingleton testSingleton) {
		this.fileChangeListener = fileChangeListener;
		this.manageToggle = manageToggle;
		this.togglesState = togglesState;
		this.testSingleton = testSingleton;
	}

	public static void main(String[] args) {
		SpringApplication.run(ConsulApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		manageToggle.getToggles();

		HashMap<String, Boolean> toggles = fileChangeListener.getToggles();

//		TogglesState.getInstance().setup(toggles);
//		var ok = testSingleton.getToggles();
//		System.out.println(ok);

		togglesState.setup(toggles);
		var memoryToggles = testSingleton.getToggles();

		System.out.println(memoryToggles);
	}









































	public void ignore() throws NotRegisteredException {
		//		Connect to Consul.
		Consul client = Consul.builder().build();
		//		Example 2: Register and check your service in with Consul.
		AgentClient agentClient = client.agentClient();

		String serviceId = "1";
		Registration service = ImmutableRegistration.builder()
				.id(serviceId)
				.name("myService")
				.port(8080)
				.check(Registration.RegCheck.ttl(3L)) // registers with a TTL of 3 seconds
				.tags(Collections.singletonList("tag1"))
				.meta(Collections.singletonMap("version", "1.0"))
				.build();

		agentClient.register(service);

		// Check in with Consul (serviceId required only).
		// Client will prepend "service:" for service level checks.
		// Note that you need to continually check in before the TTL expires, otherwise your service's state will be marked as "critical".
		agentClient.pass(serviceId);


		//Find available (healthy) services.
		HealthClient healthClient = client.healthClient();

		// Discover only "passing" nodes
		List<ServiceHealth> nodes = healthClient.getHealthyServiceInstances("Machine").getResponse();
		System.out.println(nodes);
	}
}
