package com.example.consul.api;

import org.springframework.stereotype.Component;

import java.util.EnumMap;

@Component
public class TestSingleton {


    public EnumMap<TogglesEnum, Boolean> getToggles() {
        var t =  TogglesState.getInstance().getToggles();

        return t;
    }
}
