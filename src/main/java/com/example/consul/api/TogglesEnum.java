package com.example.consul.api;

public enum TogglesEnum {
    ENABLE_COUNT_REQUESTS(false),
    ENABLE_NUMBER_OF_MODELS(false),
    ENABLE_NUMBER_OF_ITEMS(false);

    public final boolean open;

    TogglesEnum(boolean open) {
        this.open = open;
    }
}
