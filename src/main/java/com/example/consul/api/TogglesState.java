package com.example.consul.api;

import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.HashMap;

@Component
public class TogglesState {
    private static TogglesState INSTANCE;
    private EnumMap<TogglesEnum, Boolean> toggles;

    private TogglesState() {

    }

    public static TogglesState getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new TogglesState();
        }

        return INSTANCE;
    }

    public void setup(HashMap<String, Boolean> toggles) {
        try {
            this.toggles = new EnumMap<>(TogglesEnum.class);
            toggles.forEach((toggleName, open) -> this.toggles.put(TogglesEnum.valueOf(toggleName), open));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public EnumMap<TogglesEnum, Boolean> getToggles() {
        return this.toggles;
    }
}
