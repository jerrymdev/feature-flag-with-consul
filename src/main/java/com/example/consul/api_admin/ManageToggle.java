package com.example.consul.api_admin;

import com.orbitz.consul.Consul;
import com.orbitz.consul.KeyValueClient;
import org.springframework.stereotype.Component;

@Component
public class ManageToggle {
    public void getToggles() {
        try {
            //Connect to Consul.
            Consul client = Consul.builder().build();

            // Example 4: Get/Store key/values.
            KeyValueClient kvClient = client.keyValueClient();

            String value = kvClient.getValueAsString("toggles/enable_number_of_items").get(); // bar
            System.out.println(value);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
