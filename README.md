baixar imagem docker do consul

- docker pull consul

executar consul server

- docker run \
  -d \
  -p 8500:8500 \
  -p 8600:8600/udp \
  --name=badger \
  consul agent -server -ui -node=server-1 -bootstrap-expect=1 -client=0.0.0.0


executar o client que fara ponte entre api e servidor

- docker run \
  --name=fox \
  consul agent -node=client-1 -join=172.17.0.2
